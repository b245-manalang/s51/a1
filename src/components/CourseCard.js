import {Row, Col, Button, Card} from 'react-bootstrap';
import {useState} from 'react';

export default function CourseCard({courseProp}){

	const {name, description, price} = courseProp;

		//The curly braces are used for props to signify that we are providing information using expressions

	//Use the state hook for this component to be able to store its state
	//States are used to keep track of information related to individual components

		// const [getter, setter] = useState(initialGetterValue);

	const [enrollees, setEnrollees] = useState(0);

	//initial value of enrollees state
	/*	console.log(enrollees);

		setEnrollees(1);
		console.log(enrollees);
*/
	const [seats, setSeats] = useState(30);

	function enroll(){
		if (enrollees === 30){
			alert('no more seats')
		}else{
			setEnrollees(enrollees +1)
			setSeats(seats -1)
		}
	}


	return(
		<Row className = 'mt-5'>
			<Col>
		 		<Card>				      
				    <Card.Body>

		                <Card.Title>{name}</Card.Title>
		                <Card.Subtitle>Description:</Card.Subtitle>
		                <Card.Text>{description}</Card.Text>
		                <Card.Subtitle>Price:</Card.Subtitle>
		                <Card.Text>Php {price}</Card.Text>
		                <Card.Subtitle>Enrollees:</Card.Subtitle>
		                <Card.Text>{enrollees}</Card.Text>
		                 <Card.Subtitle>Available Seats:</Card.Subtitle>
		                <Card.Text>{seats}</Card.Text>
		                <Button onClick = {enroll}>Enroll Now!</Button>

				    </Card.Body>	
				</Card>
			</Col>
		</Row>
	  	);
	}